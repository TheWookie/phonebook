<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<title>Insert title here</title>
</head>
<body style="background-color: whitesmoke">
    <div style="border-style:solid; border-color:#428bca; background-color:#428bca; margin: auto;" id="header">
            <div>
                <h2 style="text-align: center; font-size: 3.8vw;">
                    Main
                </h2>
            </div>
        </div>
        
        <div id="menu-block" class="well" style="width: 20%; float: left;">
            <div id="menu">
           	 	<form style="display: inline" action="/main/contact/new" method="POST">
           	 	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                <button style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="submit">
                    <p style="font-size: 2vw;">    
                    	New Contact
                    </p>	
                </button>
                </form>
                <hr color="black" style="float: none; width: 100%"/>
                
                <form style="display: inline" action="/main/search" method="GET">
                Search
                <div>
              		By name
                	<input type="text" name="name" id="name" class="form-control" />
                </div>
                <div>
                	By surname
                	<input type="text" name="surname" id="surname" class="form-control" />
                </div>
                <div>
                	By cellphone
                	<input type="text" name="cellphone" id="cellphone" class="form-control" />
                </div>
                <button style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="submit">
                    <p style="font-size: 2vw;">    
                    	Start
                    </p>	
                </button>
                </form>
                
                <hr color="black" style="float: none; width: 100%"/>
                <form style="display: inline" action="/main/logout" method="POST">
                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                <button style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="submit">
                    <p style="font-size: 2vw;">    
                    	Logout
                    </p>	
                </button>
                </form>
            </div>
        </div>
        
        <div id="main" class="container" style="width: 60%; float: left">
            <h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    Contacts
            </h3>

            <table class="table">
                <tr> 
                    <th>Name</th>
                    <th>Surname</th>
                    <th>LastName</th>
                    <th>Cellphone</th>
                    <th>Home phone</th>
                    <th>Address</th>
                    <th>Email</th>
                    <th>Delete</th>
                    <th>Update</th>
                </tr>
                <c:forEach var="contact" items="${contacts}">
                <tr>
                    <td>${contact.name}</td>
                    <td>${contact.surname}</td>
                    <td>${contact.lastname}</td>
                    <td>${contact.cellphone}</td>
                    <td>${contact.homephone}</td>
                    <td>${contact.address}</td>
                    <td>${contact.email}</td>
                    <td>
                    	<a href="/main/contact/delete/${contact.id}">
                    		Delete
                    	</a>
                	</td>
                    <td>
                    	<a href="/main/contact/update/${contact.id}">
                    		Update
                    	</a>
                	</td>
                    <!--<td>
                    	<form action="./Controller" method="POST" class="form-signin">
                    	<input type="hidden" name="projectId" value="${project.id}"/>
                    	<button name="command" value="PROCESS_PROJECT" style="margin-top: 3%;" 
                                class="btn btn-default btn-xs" type="submit">
                            <fmt:message key="label.process"/>
                        </button>
                        </form> 
                    </td>-->
                    
                </tr>
                </c:forEach>
            </table>
        </div>         
</body>
</html>