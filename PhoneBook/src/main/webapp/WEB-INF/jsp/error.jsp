<!DOCTYPE html>
<%@ page errorPage="true" contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<html>
<head>
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <title>Error Page</title>
</head>
<body style="background-color: whitesmoke">
    <div style="border-style:solid; border-color: #e60000; background-color: #e60000; margin: auto;" id="header">
            <div>
                <h2 style="text-align: center; font-size: 3.8vw;">Error</h2>
            </div>
        </div>
        
        <div id="menu-block" class="well" style="width: 20%; float: left;">
            <button onclick="history.go(-1);return true;" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="button">
                <p style="font-size: 2vw;">        
                	Back
                </p>
            </button>
<!--                <h3 style="text-align: center; width: 100%; font-size: 2.8vw;">
                    <a onclick="history.go(-1);return true;"><fmt:message key="button.back"/></a>
                </h3>-->
        </div>
        
        <div id="main" class="container" style="width: 60%; float: left">
            <div>
                <h3 style="font-size: 2.8vw;">Error</h3>
                <h4 style="font-size: 1.8vw;">${pageContext.exception.localizedMessage}</h4> 
                ${requestScope['javax.servlet.error.message']}
            </div>
        </div> 
</body>
</html>