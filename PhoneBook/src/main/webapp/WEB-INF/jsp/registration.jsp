<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<title>Insert title here</title>
</head>
<body style="background-color: whitesmoke">
        <div style="border-style:solid; border-color:#428bca; background-color:#428bca; margin: auto;" id="header">
            <div>
                <h2 style="text-align: center; font-size: 3.8vw;">
                   Registration
                </h2>
            </div>
        </div>
        
        <div id="menu-block" class="well" style="width: 20%; float: left;">
            <div id="menu">
              	<button onclick="history.go(-1);return true;" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="button">
                    Back
                </button>
            </div>   
        </div>
    
        <div class="container">
        	<springForm:form method="POST" commandName="user" action="/registration/submit/">
        		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                <div style="width: 50%; margin-left: 25%;">
                    <h2 class="form-signin-heading">Sign in</h2>
                    <div style="margin-top: 2%;">
                        <div>Fullname<b style="color: red"> *</b>:</div>
                        <springForm:errors path="fullname" />
                        <springForm:input type="text" path="fullname" name="fullname" id="fullname" class="form-control"  />
                    </div>
                    <div style="margin-top: 2%;">
                        <div>Login<b style="color: red"> *</b>:</div>
                        <springForm:errors path="login" />
                        <springForm:input type="text" path="login" name="login" id="login" class="form-control" />
                    </div>
                    <div style="margin-top: 2%;">
                        <div>Password<b style="color: red"> *</b>:</div>
                        <springForm:errors path="password" />
                        <springForm:input type="password" path="password" name="password" id="password" class="form-control" />
                    </div>
                    
                <button style="margin-top: 3%;" class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
                </div>
        	</springForm:form>
        </div> 
</body>
</html>