<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="springForm"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- Bootstrap -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<title>Insert title here</title>
</head>
<body style="background-color: whitesmoke">
        <div style="border-style:solid; border-color:#428bca; background-color:#428bca; margin: auto;" id="header">
            <div>
                <h2 style="text-align: center; font-size: 3.8vw;">
                   New contact creation
                </h2>
            </div>
        </div>
        
        <div id="menu-block" class="well" style="width: 20%; float: left;">
            <div id="menu">
              	<button onclick="history.go(-1);return true;" style="margin-top: 3%;" 
                        class="btn btn-lg btn-default btn-block" type="button">
                    Back
                </button>
            </div>   
        </div>
    
        <div class="container">
        <springForm:form method="POST" commandName="contact" action="/main/contact/update/submit">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        <div style="width: 50%; margin-left: 25%;">
             <springForm:input type="hidden" path="id" class="form-control"  />
             <div style="margin-top: 2%;">
                <div>Name<b style="color: red"> *</b>:</div>
                <springForm:errors path="name" />
             	<springForm:input type="text" path="name" class="form-control"  />
             </div>
             <div style="margin-top: 2%;">
                <div>Surname<b style="color: red"> *</b>:</div>
                <springForm:errors path="surname" />
             	<springForm:input type="text" path="surname" class="form-control"  />
             </div>
             <div style="margin-top: 2%;">
                <div>Lastname<b style="color: red"> *</b>:</div>
                <springForm:errors path="lastname" />
             	<springForm:input type="text" path="lastname" class="form-control"  />
             </div>
             <div style="margin-top: 2%;">
                <div>Cellphone<b style="color: red"> *</b>:</div>
                <springForm:errors path="cellphone" />
             	<springForm:input type="text" path="cellphone" class="form-control"  />
             </div>
             <div style="margin-top: 2%;">
                <div>Home phone<b style="color: red"> *</b>:</div>
                <springForm:errors path="homephone" />
             	<springForm:input type="text" path="homephone" class="form-control"  />
             </div>
             <div style="margin-top: 2%;">
                <div>Address<b style="color: red"> *</b>:</div>
                <springForm:errors path="address" />
             	<springForm:input type="text" path="address" class="form-control"  />
             </div>
             <div style="margin-top: 2%;">
                <div>Email<b style="color: red"> *</b>:</div>
                <springForm:errors path="email" />
             	<springForm:input type="text" path="email" class="form-control"  />
             </div>
             
             <button style="margin-top: 3%;" class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
        </div>     
        </springForm:form>
         </div>
</body>
</html>