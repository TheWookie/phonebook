<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html xmlns:th="http://www.thymeleaf.org" xmlns:tiles="http://www.thymeleaf.org">

<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<!-- Bootstrap -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	
	
	<title>Welcome</title>
</head>
<body style="background-color: whitesmoke" onload='document.loginForm.username.focus();'>
        <div style="border-style:solid; border-color:#428bca; background-color:#428bca; margin: auto;" id="header">
            <div>
                <h2 style="text-align: center; font-size: 3.8vw;">
                   Authorization
                </h2>
            </div>
        </div>
    
        <div class="container">
            <!-- <form name='loginForm' action="<c:url value='j_spring_security_check' />"
             method='POST'>-->
             <form name="f" th:action="@{/login}" method="post">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                <div style="width: 50%; margin-left: 25%;">
                    <h2 class="form-signin-heading">Sign in</h2>
                    <div>
                        <label for="inputUsername" class="sr-only">login</label>
                        <input type="text" name="username" id="username" class="form-control" placeholder="login" required="" autofocus="">
                    </div>
                    <div style="margin-top: 1%;">
                        <label for="inputPassword" class="sr-only">password</label>
                        <input type="password" name="password" id="password" class="form-control" placeholder=password required="">
                    </div>
                    
                <button style="margin-top: 3%;" class="btn btn-lg btn-primary btn-block" type="submit">submit</button>
                <a href="/registration">Sign up</a>
                </div>
            </form>
        </div> 
</body>
</html>