package com.wookie.constants;

public interface Pages {
	String LOGIN_PAGE = "login";
	String LOGIN_PAGE_REDIRECT = "redirect:/login";
	String MAIN_PAGE = "main";
	String MAIN_PAGE_REDIRECT = "redirect:/main";
	String REGISTRATION_PAGE = "registration";
	String REGISTRATION_PAGE_REDIRECT = "redirect:/registration";
	String UPDATE_CONTACT_PAGE = "update_contact";
	String ERROR_PAGE = "error";
}
