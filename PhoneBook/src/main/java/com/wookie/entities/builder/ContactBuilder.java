package com.wookie.entities.builder;

import com.wookie.entities.Contact;
import com.wookie.entities.User;

public class ContactBuilder {
	private Contact instance = new Contact();
	
	public ContactBuilder setId(Integer id) {
		this.instance.setId(id);
		return this;
	}

	public ContactBuilder setName(String name) {
		this.instance.setName(name);
		return this;
	}

	public ContactBuilder setSurname(String surname) {
		this.instance.setSurname(surname);
		return this;
	}

	public ContactBuilder setLastname(String lastname) {
		this.instance.setLastname(lastname);
		return this;
	}

	public ContactBuilder setCellphone(String cellphone) {
		this.instance.setCellphone(cellphone);
		return this;
	}

	public ContactBuilder setHomephone(String homephone) {
		this.instance.setHomephone(homephone);
		return this;
	}

	public ContactBuilder setAddress(String address) {
		instance.setAddress(address);
		return this;
	}

	public ContactBuilder setEmail(String email) {
		this.instance.setEmail(email);
		return this;
	}
	
	public ContactBuilder setUser(User user) {
		this.instance.setUser(user);
		return this;
	}
	
	public Contact build() {
		return instance;
	}
}
