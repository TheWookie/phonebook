package com.wookie.entities.builder;


import com.wookie.entities.Contact;
import com.wookie.entities.User;

public class UserBuilder {
	private User instance = new User();

	public UserBuilder setId(Integer id) {
		this.instance.setId(id);
		return this;
	}

	public UserBuilder setFullname(String fullname) {
		this.instance.setFullname(fullname);
		return this;
	}

	public UserBuilder setLogin(String login) {
		this.instance.setLogin(login);
		return this;
	}

	public UserBuilder setPassword(String password) {
		this.instance.setPassword(password);
		return this;
	}
	
	public User build() {
		return instance;
	}
}
