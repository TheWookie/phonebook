package com.wookie.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.wookie.dao.UserDao;
import com.wookie.entities.User;

/*
 * Service for managing Spring Security. User's ID saved in UserDetails class
 * <p>for further retrieving.
 */
@Service
public class SecurityUserDataServiceImpl implements UserDetailsService {
	@Autowired
	UserDao userDao;
	
	@Override
	@Transactional(readOnly=true)
	public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
		User user = userDao.findByLogin(login);
		
		if(user != null)
			return new org.springframework.security.core.userdetails.User(
					user.getId().toString(), user.getPassword(), 
                true, true, true, true, getGrantedAuthorities(user));
		else
			throw new UsernameNotFoundException("Bad credentials");

	}
	
	private List<GrantedAuthority> getGrantedAuthorities(User user){
        List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
        
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        return authorities;
    }


	
}
