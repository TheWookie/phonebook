package com.wookie.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wookie.dao.ContactDao;
import com.wookie.entities.Contact;
import com.wookie.entities.User;
import com.wookie.entities.builder.ContactBuilder;
import com.wookie.entities.builder.UserBuilder;
import com.wookie.service.ContactService;

@Service
public class ContactServiceImpl implements ContactService {

	@Autowired
	private ContactDao contactDao;
	


	@Override
	public boolean delete(int contactId) {
		return contactDao.delete(new ContactBuilder().setId(contactId).build());
	}



	@Override
	public Contact update(String userId, Integer contactId, String name, String surname, String lastname, String cellphone, String homephone,
			String address, String email) {
		
		int uid = Integer.parseInt(userId);
		Contact contact = new ContactBuilder()
				.setId(contactId)
				.setName(name)
				.setSurname(surname)
				.setLastname(lastname)
				.setCellphone(cellphone)
				.setHomephone(homephone)
				.setAddress(address)
				.setEmail(email)
				.setUser(new UserBuilder().setId(uid).build())
				.build();
		
		return contactDao.save(contact);
	}

	
	
	@Override
	public Contact update(String userId, Contact contact) {
		int uid = Integer.parseInt(userId);
		contact.setUser(new UserBuilder().setId(uid).build());
		
		return contactDao.save(contact);
	}
	


	@Override
	public List<Contact> getByUser(String userId) {
		int uid = Integer.parseInt(userId);
		User user = new UserBuilder().setId(uid).build();
	
		return contactDao.getByUser(user);
	}



	@Override
	public Contact getById(int contactId) {
		return contactDao.getById(contactId);
	}



	@Override
	public List<Contact> search(String userId, String name, String surname, String cellphone) {
		int uid = Integer.parseInt(userId);
		User user = new UserBuilder().setId(uid).build();
		
		return contactDao.search(user, name, surname, cellphone);
	}


}
