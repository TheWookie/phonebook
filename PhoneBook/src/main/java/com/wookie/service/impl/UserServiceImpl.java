package com.wookie.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wookie.dao.UserDao;
import com.wookie.entities.User;
import com.wookie.entities.builder.ContactBuilder;
import com.wookie.entities.builder.UserBuilder;
import com.wookie.service.UserService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired 
	UserDao userDao;
	
	@Override
	public List<User> getAll() {
		return userDao.getAll();
	}

	@Override
	public void createUser(String fullname, String login, String password) {
		User user = new UserBuilder()
						.setFullname(fullname)
						.setLogin(login)
						.setPassword(password)
						.build();
		
		userDao.save(user);
	}

	@Override
	public void createUser(User user) {
		userDao.save(user);
	}
	
}
