package com.wookie.service;

import java.util.List;

import com.wookie.entities.Contact;

public interface ContactService {
	Contact update(String userId, Integer contactId, String name, String surname, String lastname, String cellphone, String homephone, 
			String address, String email);
	
	Contact update(String userId, Contact contact);

	boolean delete(int contactId);
	
	List<Contact> getByUser(String userId);
	
	Contact getById(int contactId);
	
	List<Contact> search(String userId, String name, String surname, String cellphone);
}
