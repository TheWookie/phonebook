package com.wookie.service;

import java.util.List;

import com.wookie.entities.User;

public interface UserService {
	List<User> getAll();
	
	void createUser(String fullname, String login, String password);
	
	void createUser(User user);
}
