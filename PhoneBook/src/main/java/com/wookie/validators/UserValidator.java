package com.wookie.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.wookie.entities.User;

@Component
public class UserValidator implements Validator {
	private Matcher matcher;
	
	public static int LOGIN_MIN_LENGTH = 3;
	public static int FULLNAME_MIN_LENGTH = 5;
	public static int PASSWORD_MIN_LENGTH = 5;
	
	public static final Pattern LOGIN_PATTERN =  Pattern.compile("[a-zA-Z0-9]*");
	
	@Override
	public boolean supports(Class<?> param) {
		return User.class.equals(param);
	}

	@Override
	public void validate(Object obj, Errors errors) {
		User user = (User) obj;
		
		validateLogin(user.getLogin(), errors);
		validatePassword(user.getPassword(), errors);
		validateFullname(user.getFullname(), errors);
	}
	
	private void validateLogin(String login, Errors errors) {
		
		if((login == null) || (login == ""))
			errors.rejectValue("login", "required", "Login required");
		else
			if(login.length() < LOGIN_MIN_LENGTH)
				errors.rejectValue("login", "incorrect", "Login must contains " + LOGIN_MIN_LENGTH + " symbols or more");
			else {
				matcher = LOGIN_PATTERN.matcher(login);
				if(!matcher.matches()) {
					errors.rejectValue("login", "incorrect", "Login must not contain special symbols");
				}
			}
	}

	private void validatePassword(String password, Errors errors) {
		if((password == null) || (password == ""))
			errors.rejectValue("password", "required", "Password required");
		else
			if(password.length() < PASSWORD_MIN_LENGTH)
				errors.rejectValue("password", "incorrect", 
						"Password must contains " + PASSWORD_MIN_LENGTH + " symbols or more");
	}
	
	private void validateFullname(String fullname, Errors errors) {
		if((fullname == null) || (fullname == ""))
			errors.rejectValue("fullname", "required", "Fullname required");
		else
			if(fullname.length() < FULLNAME_MIN_LENGTH)
				errors.rejectValue("fullname", "incorrect", 
						"Fullname must contains " + FULLNAME_MIN_LENGTH + " symbols or more");
	}
}
