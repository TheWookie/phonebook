package com.wookie.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.wookie.entities.Contact;

@Component
public class ContactValidator implements Validator {
	private Matcher matcher;
	
	public static final int NAME_MIN_LENGTH = 4;
	public static final int SURNAME_MIN_LENGTH = 4;
	public static final int LASTNAME_MIN_LENGTH = 4;
	
	public static final String EMAIL_HINT = "email@ukr.net";
	public static final String PHONE_NUMBER_HINT = "+380661234567";
	
	public static final Pattern EMAIL_PATTERN =  Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);
	public static final Pattern PHONE_NUMBER_PATTERN =  Pattern.compile("[+]{1}380[0-9]{9}");
	
	@Override
	public boolean supports(Class<?> clazz) {
		return Contact.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Contact contact = (Contact) target;
		
		validateName(contact.getName(), errors);
		validateSurname(contact.getSurname(), errors);
		validateLastame(contact.getLastname(), errors);
		validateCellphone(contact.getCellphone(), errors);
		validateHomephone(contact.getHomephone(), errors);
		validateAddress(contact.getAddress(), errors);
		validateEmail(contact.getEmail(), errors);
	}
	
	private void validateName(String name, Errors errors) {
		if((name == null) || (name == ""))
			errors.rejectValue("name", "required", "Name required");
		else
			if(name.length() < NAME_MIN_LENGTH)
				errors.rejectValue("name", "incorrect", "Name must contains " + NAME_MIN_LENGTH + " symbols or more");
	}
	
	private void validateSurname(String surname, Errors errors) {
		if((surname == null) || (surname == ""))
			errors.rejectValue("surname", "required", "Surname required");
		else
			if(surname.length() < SURNAME_MIN_LENGTH)
				errors.rejectValue("surname", "incorrect", "Surname must contains " + SURNAME_MIN_LENGTH + " symbols or more");
	}

	private void validateLastame(String lastname, Errors errors) {
		if((lastname == null) || (lastname == ""))
			errors.rejectValue("lastname", "required", "Lastname required");
		else
			if(lastname.length() < LASTNAME_MIN_LENGTH)
				errors.rejectValue("lastname", "incorrect", "Lastname must contains " + SURNAME_MIN_LENGTH + " symbols or more");
	}
	
	private void validateCellphone(String cellphone, Errors errors) {
		if((cellphone == null) || (cellphone == ""))
			errors.rejectValue("cellphone", "required", "Cellphone required");
		else {
			matcher = PHONE_NUMBER_PATTERN.matcher(cellphone);
			if(!matcher.matches())
				errors.rejectValue("cellphone", "incorrect", "Cellphone must match pattern: " + PHONE_NUMBER_HINT);
		}
	}
	
	private void validateHomephone(String homephone, Errors errors) {
		if((homephone != null) && (homephone != "")) {
			matcher = PHONE_NUMBER_PATTERN.matcher(homephone);
			if(!matcher.matches())
				errors.rejectValue("homephone", "incorrect", "Homephone must match pattern: " + PHONE_NUMBER_HINT);
		}
	}
	
	private void validateAddress(String address, Errors errors) {
		
	}
	
	private void validateEmail(String email, Errors errors) {
		if((email != null) && (email != "")) {
			matcher = EMAIL_PATTERN.matcher(email);
			if(!matcher.matches())
				errors.rejectValue("email", "incorrect", "Email must match pattern: " + EMAIL_HINT);
		}
	}
}


