package com.wookie.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.wookie.constants.Pages;
import com.wookie.entities.User;
import com.wookie.service.ContactService;
import com.wookie.service.UserService;


@Controller
@EnableAutoConfiguration
public class RoutingController {

	@Autowired 
	private UserService userService;
	
	@Autowired
	@Qualifier("userValidator")
	private Validator validator;
	
	@RequestMapping(value = "/login")
	public String getLoginPage() {
		return Pages.LOGIN_PAGE;
	}
	
	@RequestMapping(value = "/")
	public String routeMainPage() {
		System.out.println("Main1");

		return Pages.MAIN_PAGE_REDIRECT;
	}

	@RequestMapping(value = "/registration")
	public String getRegistrationPage(Model model) {
		model.addAttribute("user", new User());
		
		return Pages.REGISTRATION_PAGE;
	}
	
	@RequestMapping(value = "/registration/submit", method = RequestMethod.POST) 
	public String registrationSubmit(
			@ModelAttribute("user") @Validated  User user,
			BindingResult bindingResult
			) {
		
		if (bindingResult.hasErrors()) {
			return Pages.REGISTRATION_PAGE;
		}
		
		userService.createUser(user);
		
		return Pages.LOGIN_PAGE_REDIRECT;
	}
	
	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
	
}




