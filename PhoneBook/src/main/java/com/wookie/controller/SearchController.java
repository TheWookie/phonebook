package com.wookie.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.wookie.constants.Pages;
import com.wookie.service.ContactService;

@Controller
@EnableAutoConfiguration
public class SearchController {

	@Autowired
	private ContactService contactService;
	
	
	@RequestMapping(value = "/main/search", method= RequestMethod.GET)
	public String contactSearch(
			Model model,
			Principal principal,
			@RequestParam String name,
			@RequestParam String surname,
			@RequestParam String cellphone
			) {
		
		model.addAttribute("contacts", contactService.search(principal.getName(), 
				name, surname, cellphone));

		return Pages.MAIN_PAGE;
	}
}
