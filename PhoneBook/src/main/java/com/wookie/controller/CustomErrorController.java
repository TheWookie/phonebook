package com.wookie.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.wookie.constants.Pages;

@Controller
public class CustomErrorController {

	@ExceptionHandler(Exception.class)
	public ModelAndView handleAllException(Exception ex) {
		ModelAndView model = new ModelAndView(Pages.ERROR_PAGE);

		return model;

	}
	
}
