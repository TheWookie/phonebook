package com.wookie.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.wookie.constants.Pages;
import com.wookie.entities.Contact;
import com.wookie.entities.User;
import com.wookie.service.ContactService;
import com.wookie.service.UserService;

@Controller
@EnableAutoConfiguration
public class MainController {
	
	@Autowired
	private ContactService contactService;
	
	@Autowired
	@Qualifier("contactValidator")
	private Validator validator;
	
	@RequestMapping(value = "/main/logout")
	public String processLogout(Authentication auth) {
    	if(auth != null) {
    		SecurityContextHolder.getContext().setAuthentication(null);
    	}

		return Pages.MAIN_PAGE_REDIRECT;
	}
	
	@RequestMapping(value = "/main")
	public String getMainPage(Model model, Principal principal) {
		model.addAttribute("contacts", contactService.getByUser(principal.getName()));
		
		return Pages.MAIN_PAGE;
	}
	
	@RequestMapping(value = "/main/contact/new")
	public String getNewContactCreationPage(Model model) {
		model.addAttribute("contact", new Contact());
		
		return Pages.UPDATE_CONTACT_PAGE;
	}
	
	@RequestMapping(value = "main/contact/update/submit", method = RequestMethod.POST)
	public String updateContact(
			Principal principal,
			@ModelAttribute("contact") @Validated Contact contact,
			BindingResult bindingResult
			) {

		if (bindingResult.hasErrors()) {
			return Pages.UPDATE_CONTACT_PAGE;
		}
		
		contactService.update(principal.getName(), contact);
		return Pages.MAIN_PAGE_REDIRECT;
	}
	
	@RequestMapping(value = "/main/contact/delete/{contactId}")
	public String deleteContact(@PathVariable int contactId) {
		contactService.delete(contactId);
		
		return Pages.MAIN_PAGE_REDIRECT;
	}
	
	@RequestMapping(value = "/main/contact/update/{contactId}")
	public String getContactUpdatePage(Model model, @PathVariable int contactId) {
		model.addAttribute("contact", contactService.getById(contactId));
		
		return Pages.UPDATE_CONTACT_PAGE;
	}
	
	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}
}




