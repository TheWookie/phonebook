package com.wookie.fileparsers.json;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wookie.entities.User;

public class JsonFileParser<T> {
	private String filePath;
	
	public JsonFileParser(String filePath) {
		this.filePath = filePath;
	}
	
	
	/**
	 * Method rewrite file with new collection using Jackson.
	 * @param toPersist collection to persist.
	 */
	public void persistNewCollection(Collection<T> toPersist) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.writeValue(new FileWriter(filePath), toPersist);
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
	
	public Set<T> readFile(Class c) {
        try {
        	ObjectMapper mapper = new ObjectMapper();
        	JavaType type = mapper.getTypeFactory().constructCollectionType(TreeSet.class, c);
        	Set<T> result = mapper.readValue(new File(filePath), type);
			return result;
        } catch (FileNotFoundException e) {
        	persistNewCollection(new TreeSet<T>());
        	return readFile(c);
		} catch (IOException e) {
			throw new RuntimeException(e.getMessage());
		}
	}
}
