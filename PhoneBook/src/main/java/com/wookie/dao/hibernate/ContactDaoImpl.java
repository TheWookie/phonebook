package com.wookie.dao.hibernate;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import com.wookie.dao.ContactDao;
import com.wookie.entities.Contact;
import com.wookie.entities.User;
import com.wookie.entities.builder.UserBuilder;

public class ContactDaoImpl implements ContactDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<Contact> getAll() {
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Contact.class);
		
		return criteria.list();
	}

	@Override
	public Contact getById(int id) {
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Contact.class)
							.add(Restrictions.eq("id", id));
		
		return (Contact) criteria.uniqueResult();
	}

	@Override
	public Contact save(Contact obj) {
		entityManager.merge(obj);
		return obj;
	}

	@Override
	public boolean delete(Contact obj) {
		Contact credential = getById(obj.getId());
		entityManager.remove(credential);
		return true;
	}

	@Override
	public List<Contact> getByUser(User user) {
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Contact.class)
							.add(Restrictions.eq("user", user));
		
		return criteria.list();
	}

	@Override
	public List<Contact> search(User user, String name, String surname, String cellphone) {
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Contact.class)
				.add(Restrictions.eq("user", user))
				.add(Restrictions.like("name", name, MatchMode.ANYWHERE).ignoreCase())
				.add(Restrictions.like("surname", surname, MatchMode.ANYWHERE).ignoreCase())
				.add(Restrictions.like("cellphone", cellphone, MatchMode.ANYWHERE));
		
		return criteria.list();
	}


}



