package com.wookie.dao.hibernate;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import com.wookie.dao.UserDao;
import com.wookie.entities.User;

public class UserDaoImpl implements UserDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<User> getAll() {
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(User.class);
		
		return criteria.list();
	}

	@Override
	public User getById(int id) {
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(User.class)
							.add(Restrictions.eq("id", id));
		
		return (User) criteria.uniqueResult();
	}

	@Override
	public User save(User obj) throws RuntimeException {
		try {
			entityManager.persist(obj);
			return obj;
		} catch(Exception e) {
			throw new RuntimeException("Such user is already exists.");
		}
	}

	@Override
	public boolean delete(User obj) {
		User user = getById(obj.getId());
		entityManager.remove(user);
		return true;
	}

	@Override
	public User findByLogin(String login) {
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(User.class)
							.add(Restrictions.eq("login", login));
		
		return (User) criteria.uniqueResult();
	}

}
