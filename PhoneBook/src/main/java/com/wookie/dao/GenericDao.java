package com.wookie.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;


public interface GenericDao<T> {
	
	@Transactional(readOnly=true)
	T getById(int id);

	@Transactional(readOnly=true)
	List<T> getAll();

	/**
	 * Method updates entity if object's ID is not null.
	 * Method creates new entity if object's ID is null.
	 * If object's ID is not null but such object doesn't exists in database, 
	 * method creates new entry in database with new ID.
	 * @param obj - object to persist.
	 * @return new or updated object with all parameters.
	 * @throws RuntieException if such user is already exists.
	 */
	@Transactional
	T save (T obj) throws RuntimeException;

	/**
	 * Method removes entity from database if such entity exists.
	 * 
	 * @param obj - object to delete.
	 * @return If removing takes place method returns true. False if not.
	 */
	@Transactional
	boolean delete(T obj);
}

