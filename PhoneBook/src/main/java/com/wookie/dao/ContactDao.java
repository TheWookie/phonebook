package com.wookie.dao;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.wookie.entities.Contact;
import com.wookie.entities.User;


public interface ContactDao extends GenericDao<Contact> {
	
	@Transactional(readOnly=true)
	List<Contact> getByUser(User user);
	
	/**
	 * Method search contacts with specified parameters. 
	 * If parameter "user" is null, method wouldn't find anything.
	 * If parameters "name", "surname" or "cellphone" are empty, they won't take place in searching.
	 * @param user {@link com.wookie.entities.User} object.
	 * @param name
	 * @param surname
	 * @param cellphone
	 * @return {@link java.util.List} of {@link com.wookie.entities.Contact} objects.
	 */
	@Transactional(readOnly=true)
	List<Contact> search(User user, String name, String surname, String cellphone);
}
