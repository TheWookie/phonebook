package com.wookie.dao.json;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Value;

import com.wookie.dao.UserDao;
import com.wookie.entities.User;
import com.wookie.fileparsers.json.JsonFileParser;

public class UserDaoImpl implements UserDao {
	@Value("${file.users}")
	private String fileName;
	@Value("${folder.path}")
	private String filePath;
	private JsonFileParser<User> parser;
	
	@PostConstruct
    public void postConstruct() {
        parser = new JsonFileParser<>(filePath + fileName);
    }
	
	@Override
	public User getById(int id) {
		Set<User> users = parser.readFile(User.class);
		
		for(User user : users) {
			if(user.getId().equals(id)) 
				return user;
		}
		
		return null;
	}

	
	@Override
	public List<User> getAll() {
		return new ArrayList<>(parser.readFile(User.class));
	}

	/**
	 * Method updates an entry in database with ID value. If entry with
	 * such ID isn't exists, method creates new entry with new ID.
	 * @param obj {@link com.wookie.entities.User} object to persist.
	 * @return persisted object with all parameters including ID. 
	 */
	private User create(User obj) {
		List<User> users = new ArrayList<>(parser.readFile(User.class));
		int lastId;
		
		if(!users.isEmpty())
			lastId = users.get(users.size() - 1).getId();
		else
			lastId = 0;
		obj.setId(lastId + 1);
		
		users.add(obj);
		parser.persistNewCollection(users);
		
		return obj;
	}
	
	
	private User update(User obj) {
		Set<User> users = parser.readFile(User.class);
		
		if(users.contains(obj))
			for(User user : users) {
				if(user.getId().equals(obj.getId())) {
					User merged = user.merge(obj);;
				
					users.remove(user);
					users.add(merged);
					parser.persistNewCollection(users);
				
					return merged;
				}
			}
		
		return create(obj);
	}
	
	
	@Override
	public User save(User obj) throws RuntimeException { 
		if(findByLogin(obj.getLogin()) != null)
			throw new RuntimeException("Such user is already exists.");
		if(obj.getId() == null) {
			return create(obj);
		} else {
			return update(obj);
		}
	}

	
	@Override
	public boolean delete(User obj) {
		Set<User> users = parser.readFile(User.class);
		
		if(users.contains(obj)) {
			users.remove(obj);
			parser.persistNewCollection(users);
			return true;
		} else {
			return false;
		}
		
	}

	
	@Override
	public User findByLogin(String login) {
		Set<User> users = parser.readFile(User.class);
		
		for(User user : users) {
			if(login.equals(user.getLogin()))
				return user;
		}
		
		return null;
	}

	
	public JsonFileParser<User> getParser() {
		return parser;
	}

	public void setParser(JsonFileParser<User> parser) {
		this.parser = parser;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
