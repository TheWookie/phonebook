package com.wookie.dao.json;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;

import com.wookie.dao.ContactDao;
import com.wookie.entities.Contact;
import com.wookie.entities.User;
import com.wookie.fileparsers.json.JsonFileParser;

public class ContactDaoImpl implements ContactDao {
	@Value("${file.contacts}")
	private String fileName;
	@Value("${folder.path}")
	private String filePath;
	private JsonFileParser<Contact> parser;
	
	@PostConstruct
    public void postConstruct() {
        parser = new JsonFileParser<>(filePath + fileName);
    }
	
	@Override
	public Contact getById(int id) {
		Set<Contact> contacts = parser.readFile(Contact.class);
		
		for(Contact contact : contacts) {
			if(contact.getId().equals(id)) 
				return contact;
		}
		
		return null;
	}

	@Override
	public List<Contact> getAll() {
		return new ArrayList<>(parser.readFile(Contact.class));
	}

	/**
	 * Method creates an entry in database with generated ID value.
	 * @param obj object to persist.
	 * @return created object with all parameters including ID. 
	 */
	private Contact create(Contact obj) {
		List<Contact> contacts = new ArrayList<>(parser.readFile(Contact.class)); 
		int lastId;
		
		if(!contacts.isEmpty())
			lastId = contacts.get(contacts.size() - 1).getId();
		else
			lastId = 0;
		obj.setId(lastId + 1);
		
		contacts.add(obj);
		parser.persistNewCollection(contacts);
		
		return obj;
	}
	
	/**
	 * Method updates an entry in database with ID value. If entry with
	 * such ID isn't exists, method creates new entry with new ID.
	 * @param obj {@link com.wookie.entities.Contact} object to persist.
	 * @return persisted object with all parameters including ID. 
	 */
	private Contact update(Contact obj) {
		Set<Contact> contacts = parser.readFile(Contact.class);
		
		if(contacts.contains(obj))
			for(Contact contact : contacts) {
				if(contact.getId().equals(obj.getId())) {
					Contact merged = contact.merge(obj);;
				
					contacts.remove(contact);
					contacts.add(merged);
					parser.persistNewCollection(contacts);
				
					return merged;
				}
			}
		
		return create(obj);
	}
	
	@Override
	public Contact save(Contact obj) {
		if(obj.getId() == null) {
			return create(obj);
		} else {
			return update(obj);
		}
	}

	@Override
	public boolean delete(Contact obj) {
		Set<Contact> contacts = parser.readFile(Contact.class);
		
		if(contacts.contains(obj)) {
			contacts.remove(obj);
			parser.persistNewCollection(contacts);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<Contact> getByUser(User user) {
		Set<Contact> contacts = parser.readFile(Contact.class);
		List<Contact> result = new ArrayList<>();
		
		for(Contact contact : contacts) {
			if(contact.getUser().equals(user)) 
				result.add(contact);
		}
		
		return result;
	}
	
	@Override
	public List<Contact> search(User user, String name, String surname, String cellphone) {
		List<Contact> result = getByUser(user);
		
		if((name != null) && (name != "")) 
			result = result.stream().filter(contact -> name.equals(contact.getName()))
			.collect(Collectors.toList());
		if((surname != null) && (surname != ""))
			result = result.stream().filter(contact -> surname.equals(contact.getSurname()))
					.collect(Collectors.toList());
		if((cellphone != null) && (cellphone != ""))
			result = result.stream().filter(contact -> cellphone.equals(contact.getCellphone()))
					.collect(Collectors.toList());
		
		return result;
	}
	
	
	public JsonFileParser<Contact> getParser() {
		return parser;
	}

	public void setParser(JsonFileParser<Contact> parser) {
		this.parser = parser;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	
}
