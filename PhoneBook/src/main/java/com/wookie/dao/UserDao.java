package com.wookie.dao;

import org.springframework.transaction.annotation.Transactional;

import com.wookie.entities.User;

public interface UserDao extends GenericDao<User> {
	
	@Transactional(readOnly=true)
	User findByLogin(String login);

}
