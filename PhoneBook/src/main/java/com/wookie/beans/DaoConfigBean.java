package com.wookie.beans;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.wookie.dao.*;

@Configuration
@ComponentScan(value = {"com.wookie.dao"})
public class DaoConfigBean {
	@Value("${project.user.dao.class.name}")
	private String userDaoName;
	@Value("${project.contact.dao.class.name}")
	private String contactDaoName;
	@Value("${project.datasource}")
	private String implementationName;
	@Value("${project.dao.package}")
	private String packagePath;
	
	@Bean
	public UserDao getUserDao() 
			throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		return (UserDao) Class.forName(packagePath + implementationName + userDaoName).newInstance();
	}
	
	@Bean 
	public ContactDao getContactDao() 
			throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		return (ContactDao) Class.forName(packagePath + implementationName + contactDaoName).newInstance();
	}
	
}
