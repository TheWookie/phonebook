package com.wookie.dao.json;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.wookie.entities.Contact;
import com.wookie.entities.User;
import com.wookie.entities.builder.ContactBuilder;
import com.wookie.entities.builder.UserBuilder;
import com.wookie.fileparsers.json.JsonFileParser;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ContactDaoJsonImplTest {
	private ContactDaoImpl dao = new ContactDaoImpl();
	private JsonFileParser<Contact> parser = mock(JsonFileParser.class);
	private Set<Contact> contacts = new TreeSet<>();
	private Contact contact1 = new ContactBuilder()
			.setId(1).setName("name1").setSurname("surname1").setLastname("lastname1")
			.setCellphone("12345").setHomephone("54321")
			.setAddress("address1").setEmail("email1")
			.setUser(new UserBuilder().setId(1).build())
			.build();
	private Contact contact2 = new ContactBuilder()
			.setId(2).setName("name2").setSurname("surname2").setLastname("lastname2")
			.setCellphone("12345").setHomephone("54321")
			.setAddress("address2").setEmail("email2")
			.setUser(new UserBuilder().setId(1).build())
			.build();
	private Contact contact3 = new ContactBuilder()
			.setId(3).setName("name3").setSurname("surname3").setLastname("lastname3")
			.setCellphone("12345").setHomephone("54321")
			.setAddress("address3").setEmail("email3")
			.setUser(new UserBuilder().setId(2).build())
			.build();
	
	@Before
	public void beforeClass() {
		contacts.add(contact1);
		contacts.add(contact2);
		contacts.add(contact3);
		
		when(parser.readFile(Contact.class))
		.thenReturn(contacts);
		
		dao.setParser(parser);
	}
	
	@Test
	public void getAllTest() {
		assertEquals(new ArrayList<>(contacts), dao.getAll());
	}
	
	@Test
	public void getAllWithEmptyFileTest() {
		Set<Contact> empty = new TreeSet<>();
		
		when(parser.readFile(Contact.class))
		.thenReturn(empty);
		assertEquals(new ArrayList<>(), dao.getAll());
	}
	
	@Test
	public void getByIdTest() {
		assertEquals(contact1, dao.getById(1));
	}
	
	
	@Test
	public void saveExistedTest() {
		Contact contact = new ContactBuilder()
				.setId(3)
				.setName("name3").setSurname("surname3").setLastname("lastname3")
				.setCellphone("12345").setHomephone("54321")
				.setAddress("update").setEmail("update")
				.setUser(new UserBuilder().setId(2).build())
				.build();
		
		assertEquals(contact, dao.save(contact));
	}
	
	@Test
	public void saveNewWithoutIdTest() {
		Contact contact = new ContactBuilder()
				.setName("new").setSurname("new").setLastname("new")
				.setCellphone("12345").setHomephone("54321")
				.setAddress("new").setEmail("new")
				.setUser(new UserBuilder().setId(2).build())
				.build();
		
		Contact result = dao.save(contact);
	
		contact.setId(4);
		
		assertEquals(contact, result);
	}
	
	@Test
	public void saveNewWithIdTest() {
		Contact contact = new ContactBuilder()
				.setId(10)
				.setName("new").setSurname("new").setLastname("new")
				.setCellphone("12345").setHomephone("54321")
				.setAddress("new").setEmail("new")
				.setUser(new UserBuilder().setId(2).build())
				.build();
		
		Contact result = dao.save(contact);
		
		contact.setId(4);
		
		assertEquals(contact, result);
	}
	
	@Test
	public void deleteExistedTest() {
		Contact contact = new ContactBuilder()
				.setId(1)
				.build();
		
		assertTrue(dao.delete(contact));
	}
	
	@Test
	public void deleteUnexistedTest() {
		Contact contact = new ContactBuilder()
				.setId(10)
				.build();
		
		assertFalse(dao.delete(contact));
	}
	
	@Test
	public void getByUserTest() {
		User user = new UserBuilder().setId(1).build();
		List<Contact> expected = new ArrayList<>();
		expected.add(contact1);
		expected.add(contact2);
		
		assertEquals(expected, dao.getByUser(user));
	}
	
	@Test
	public void searchByNameTest() {
		User user = new UserBuilder().setId(1).build();
		String name = "name1";
		
		List<Contact> expected = new ArrayList<>();
		expected.add(contact1);
		
		assertEquals(expected, dao.search(user, name, null, null));
	}
	
	@Test
	public void searchBySurnameTest() {
		User user = new UserBuilder().setId(1).build();
		String surname = "surname2";
		
		List<Contact> expected = new ArrayList<>();
		expected.add(contact2);
		
		assertEquals(expected, dao.search(user, null, surname, null));
	}
	
	@Test
	public void searchByCellphoneTest() {
		User user = new UserBuilder().setId(1).build();
		String cellphone = "12345";
		
		List<Contact> expected = new ArrayList<>();
		expected.add(contact1);
		expected.add(contact2);
		
		assertEquals(expected, dao.search(user, null, null, cellphone));
	}
	
	@Test
	public void searchBySurnameAndCellphoneTest() {
		User user = new UserBuilder().setId(1).build();
		String surname = "surname2";
		String cellphone = "12345";
		
		List<Contact> expected = new ArrayList<>();
		expected.add(contact2);
		
		assertEquals(expected, dao.search(user, null, surname, cellphone));
	}
}



