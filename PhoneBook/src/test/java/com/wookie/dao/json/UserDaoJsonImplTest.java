package com.wookie.dao.json;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.wookie.entities.User;
import com.wookie.entities.builder.UserBuilder;
import com.wookie.fileparsers.json.JsonFileParser;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserDaoJsonImplTest {
	private UserDaoImpl dao = new UserDaoImpl(); 
	private JsonFileParser<User> parser = mock(JsonFileParser.class);
	
	private User user1 = new UserBuilder()
			.setId(1)
			.setFullname("name1")
			.setLogin("login1")
			.setPassword("pass1")
			.build();
	private User user2 = new UserBuilder()
			.setId(2)
			.setFullname("name2")
			.setLogin("login2")
			.setPassword("pass2")
			.build();
	private Set<User> users = new TreeSet<>();
	
	@Before
	public void before() {
		users.add(user1);
		users.add(user2);
		
		when(parser.readFile(User.class))
		.thenReturn(users);
		
		dao.setParser(parser);
	}
	
	@Test
	public void getAllTest() {
		assertEquals(new ArrayList<>(users), dao.getAll());
	}
	
	@Test
	public void getAllWithEmptyFileTest() {
		Set<User> empty = new TreeSet<>();
		
		when(parser.readFile(User.class))
		.thenReturn(empty);
		assertEquals(new ArrayList<>(), dao.getAll());
	}
	
	@Test
	public void getByIdTest() {
		assertEquals(user1, dao.getById(1));
	}
	
	@Test
	public void getByLoginTest() {
		assertEquals(user2, dao.findByLogin("login2"));
	}
	
	@Test
	public void saveExistedTest() {
		User user = new UserBuilder()
				.setId(1)
				.setFullname("updated")
				.setLogin("updated")
				.setPassword("updated")
				.build();
		
		assertEquals(user, dao.save(user));
	}
	
	@Test
	public void saveNewWithoutIdTest() {
		User user = new UserBuilder()
				.setFullname("new")
				.setLogin("new")
				.setPassword("new")
				.build();
		
		User result = dao.save(user);
	
		user.setId(3);
		
		assertEquals(user, result);
	}
	
	@Test(expected = RuntimeException.class)
	public void saveNewWithoutIdWithExistedLoginTest() {
		User user = new UserBuilder()
				.setFullname("name1")
				.setLogin("login1")
				.setPassword("pass1")
				.build();
		
		User result = dao.save(user);
	}
	
	@Test
	public void saveNewWithIdTest() {
		User user = new UserBuilder()
				.setId(5)
				.setFullname("UPDATE2")
				.setLogin("some Logine")
				.setPassword("pass")
				.build();
		
		User result = dao.save(user);
		
		user.setId(3);
		
		assertEquals(user, result);
	}
	
	@Test
	public void deleteExistedTest() {
		User user = new UserBuilder()
				.setId(1)
				.build();
		
		assertTrue(dao.delete(user));
	}
	
	@Test
	public void deleteUnexistedTest() {
		User user = new UserBuilder()
				.setId(5)
				.build();
		
		assertFalse(dao.delete(user));
	}
}




