package com.wookie.jsonparser;

import java.util.LinkedHashSet;
import java.util.Set;


import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.wookie.entities.Contact;
import com.wookie.entities.User;
import com.wookie.entities.builder.ContactBuilder;
import com.wookie.entities.builder.UserBuilder;
import com.wookie.fileparsers.json.JsonFileParser;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JsonFileParserTest {
	
	@Test
	@Ignore
	public void persistUserCollectionTest() {
		JsonFileParser<User> parser = new JsonFileParser<>("userTest.json");
		User user1 = new UserBuilder()
				.setId(1)
				.setFullname("fullnaaame")
				.setLogin("some Logine")
				.setPassword("pass")
				.build();
		User user2 = new UserBuilder()
				.setId(2)
				.setFullname("fallnaaame")
				.setLogin("somaaa Logine")
				.setPassword("passds")
				.build();
		
		Set<User> collection = new LinkedHashSet<>();
		collection.add(user1);
		collection.add(user2);
		collection.add(user2);
		System.out.println("Collection : " + collection);
		
		parser.persistNewCollection(collection);
	}
	
	@Test
	@Ignore
	public void persistContactCollectionTest() {
		JsonFileParser<Contact> parser = new JsonFileParser<>("contactTest.json");

		Contact contact1 = new ContactBuilder()
				.setId(1).setName("name1").setSurname("surname1").setLastname("lastname1")
				.setCellphone("12345").setHomephone("54321")
				.setAddress("address1").setEmail("email1")
				.setUser(new UserBuilder().setId(1).build())
				.build();
		Contact contact2 = new ContactBuilder()
				.setId(2).setName("name2").setSurname("surname2").setLastname("lastname2")
				.setCellphone("12345").setHomephone("54321")
				.setAddress("address2").setEmail("email2")
				.setUser(new UserBuilder().setId(1).build())
				.build();
		
		Set<Contact> collection = new LinkedHashSet<>();
		collection.add(contact1);
		collection.add(contact2);
		System.out.println("Collection : " + collection);
		
		parser.persistNewCollection(collection);
	}
	
	@Test
	@Ignore
	public void readContactTest() {
		JsonFileParser<Contact> parser = new JsonFileParser<>("contactTest.json");
		System.out.println("Result: " + parser.readFile(Contact.class));
	}
	
	@Test
	@Ignore
	public void readUserTest() {
		JsonFileParser<User> parser = new JsonFileParser<>("userTest.json");
		System.out.println("Result: " + parser.readFile(User.class));
	}
}
