package com.wookie.validators;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.Errors;

import com.wookie.entities.Contact;
import com.wookie.entities.User;
import com.wookie.entities.builder.ContactBuilder;
import com.wookie.entities.builder.UserBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ContactValidatorTest {
	private ContactValidator validator = new ContactValidator();
	private Errors errors = mock(Errors.class);
	
	@Test
	public void successTestWithAllParameters() {
		Contact contact = new ContactBuilder()
				.setName("1234")
				.setSurname("1234")
				.setLastname("1234")
				.setCellphone(ContactValidator.PHONE_NUMBER_HINT)
				.setHomephone(ContactValidator.PHONE_NUMBER_HINT)
				.setAddress("any")
				.setEmail("email@ukr.net")
				.build();
		
		validator.validate(contact, errors);
		
		verify(errors, times(0)).rejectValue(anyObject(), anyObject(), anyObject());
	}
	
	@Test
	public void successTestOnlyWithRequiredParameters() {
		Contact contact = new ContactBuilder()
				.setName("1234")
				.setSurname("1234")
				.setLastname("1234")
				.setCellphone(ContactValidator.PHONE_NUMBER_HINT)
				.build();
		
		validator.validate(contact, errors);
		
		verify(errors, times(0)).rejectValue(anyObject(), anyObject(), anyObject());
	}
	
	@Test
	public void emptyNameTest() {
		Contact contact = new ContactBuilder()
				.setName("")
				.setSurname("1234")
				.setLastname("1234")
				.setCellphone(ContactValidator.PHONE_NUMBER_HINT)
				.setHomephone(ContactValidator.PHONE_NUMBER_HINT)
				.setAddress("any")
				.setEmail("email@ukr.net")
				.build();
		
		validator.validate(contact, errors);
		
		verify(errors, times(1)).rejectValue(eq("name"), eq("required"), anyObject());
		verify(errors, times(1)).rejectValue(anyObject(), anyObject(), anyObject());
	}
	
	@Test
	public void incorrectNameNotEnoughCharacterTest() {
		Contact contact = new ContactBuilder()
				.setName("12")
				.setSurname("1234")
				.setLastname("1234")
				.setCellphone(ContactValidator.PHONE_NUMBER_HINT)
				.setHomephone(ContactValidator.PHONE_NUMBER_HINT)
				.setAddress("any")
				.setEmail("email@ukr.net")
				.build();
		
		validator.validate(contact, errors);
		
		verify(errors, times(1)).rejectValue(eq("name"), eq("incorrect"), anyObject());
		verify(errors, times(1)).rejectValue(anyObject(), anyObject(), anyObject());
	}
	
	@Test
	public void emptySurnameTest() {
		Contact contact = new ContactBuilder()
				.setName("1234")
				.setSurname("")
				.setLastname("1234")
				.setCellphone(ContactValidator.PHONE_NUMBER_HINT)
				.setHomephone(ContactValidator.PHONE_NUMBER_HINT)
				.setAddress("any")
				.setEmail("email@ukr.net")
				.build();
		
		validator.validate(contact, errors);
		
		verify(errors, times(1)).rejectValue(eq("surname"), eq("required"), anyObject());
		verify(errors, times(1)).rejectValue(anyObject(), anyObject(), anyObject());
	}
	
	@Test
	public void incorrectSurnameNotEnoughCharacterTest() {
		Contact contact = new ContactBuilder()
				.setName("1234")
				.setSurname("12")
				.setLastname("1234")
				.setCellphone(ContactValidator.PHONE_NUMBER_HINT)
				.setHomephone(ContactValidator.PHONE_NUMBER_HINT)
				.setAddress("any")
				.setEmail("email@ukr.net")
				.build();
		
		validator.validate(contact, errors);
		
		verify(errors, times(1)).rejectValue(eq("surname"), eq("incorrect"), anyObject());
		verify(errors, times(1)).rejectValue(anyObject(), anyObject(), anyObject());
	}
	
	@Test
	public void emptyLastnameTest() {
		Contact contact = new ContactBuilder()
				.setName("1234")
				.setSurname("1234")
				.setLastname("")
				.setCellphone(ContactValidator.PHONE_NUMBER_HINT)
				.setHomephone(ContactValidator.PHONE_NUMBER_HINT)
				.setAddress("any")
				.setEmail("email@ukr.net")
				.build();
		
		validator.validate(contact, errors);
		
		verify(errors, times(1)).rejectValue(eq("lastname"), eq("required"), anyObject());
		verify(errors, times(1)).rejectValue(anyObject(), anyObject(), anyObject());
	}
	
	@Test
	public void incorrectLastameNotEnoughCharacterTest() {
		Contact contact = new ContactBuilder()
				.setName("1234")
				.setSurname("1234")
				.setLastname("12")
				.setCellphone(ContactValidator.PHONE_NUMBER_HINT)
				.setHomephone(ContactValidator.PHONE_NUMBER_HINT)
				.setAddress("any")
				.setEmail("email@ukr.net")
				.build();
		
		validator.validate(contact, errors);
		
		verify(errors, times(1)).rejectValue(eq("lastname"), eq("incorrect"), anyObject());
		verify(errors, times(1)).rejectValue(anyObject(), anyObject(), anyObject());
	}
	
	@Test
	public void emptyCellphoneTest() {
		Contact contact = new ContactBuilder()
				.setName("1234")
				.setSurname("1234")
				.setLastname("1234")
				.setCellphone("")
				.setHomephone(ContactValidator.PHONE_NUMBER_HINT)
				.setAddress("any")
				.setEmail("email@ukr.net")
				.build();
		
		validator.validate(contact, errors);
		
		verify(errors, times(1)).rejectValue(eq("cellphone"), eq("required"), anyObject());
		verify(errors, times(1)).rejectValue(anyObject(), anyObject(), anyObject());
	}
	
	@Test
	public void incorrectCellphoneTest() {
		Contact contact = new ContactBuilder()
				.setName("1234")
				.setSurname("1234")
				.setLastname("1234")
				.setCellphone("asdczx")
				.setHomephone(ContactValidator.PHONE_NUMBER_HINT)
				.setAddress("any")
				.setEmail("email@ukr.net")
				.build();
		
		validator.validate(contact, errors);
		
		verify(errors, times(1)).rejectValue(eq("cellphone"), eq("incorrect"), anyObject());
		verify(errors, times(1)).rejectValue(anyObject(), anyObject(), anyObject());
	}
	
	@Test
	public void incorrectHomephoneTest() {
		Contact contact = new ContactBuilder()
				.setName("1234")
				.setSurname("1234")
				.setLastname("1234")
				.setCellphone(ContactValidator.PHONE_NUMBER_HINT)
				.setHomephone("azxc")
				.setAddress("any")
				.setEmail("email@ukr.net")
				.build();
		
		validator.validate(contact, errors);
		
		verify(errors, times(1)).rejectValue(eq("homephone"), eq("incorrect"), anyObject());
		verify(errors, times(1)).rejectValue(anyObject(), anyObject(), anyObject());
	}
	
	@Test
	public void incorrectEmailTest() {
		Contact contact = new ContactBuilder()
				.setName("1234")
				.setSurname("1234")
				.setLastname("1234")
				.setCellphone(ContactValidator.PHONE_NUMBER_HINT)
				.setHomephone(ContactValidator.PHONE_NUMBER_HINT)
				.setAddress("any")
				.setEmail("incorrect")
				.build();
		
		validator.validate(contact, errors);
		
		verify(errors, times(1)).rejectValue(eq("email"), eq("incorrect"), anyObject());
		verify(errors, times(1)).rejectValue(anyObject(), anyObject(), anyObject());
	}
}




