package com.wookie.validators;

import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.Errors;

import com.wookie.entities.User;
import com.wookie.entities.builder.UserBuilder;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserValidatorTest {
	private UserValidator validator = new UserValidator();
	private Errors errors = mock(Errors.class);
	
	@Test
	public void correctDataTest() {
		User user = new UserBuilder()
				.setFullname("12345")
				.setLogin("abc123")
				.setPassword("12345")
				.build();
		
		validator.validate(user, errors);
		
		verify(errors, times(0)).rejectValue(anyObject(), anyObject(), anyObject());
	}
	
	
	@Test
	public void emptyLoginTest() {
		User user = new UserBuilder()
				.setFullname("12345")
				.setLogin("")
				.setPassword("12345")
				.build();
		
		validator.validate(user, errors);
		
		verify(errors, times(1)).rejectValue(eq("login"), eq("required"), anyObject());
		verify(errors, times(1)).rejectValue(anyObject(), anyObject(), anyObject());
	}

	
	@Test
	public void incorrectLoginNotEnoughCharacterTest() {
		User user = new UserBuilder()
				.setFullname("12345")
				.setLogin("1")
				.setPassword("12345")
				.build();
		
		validator.validate(user, errors);
		
		verify(errors, times(1)).rejectValue(eq("login"), eq("incorrect"), anyObject());
		verify(errors, times(1)).rejectValue(anyObject(), anyObject(), anyObject());
	}
	
	
	@Test
	public void incorrectLoginForbiddenCharacterTest() {
		User user = new UserBuilder()
				.setFullname("12345")
				.setLogin("11234@5")
				.setPassword("12345")
				.build();
		
		validator.validate(user, errors);
		
		verify(errors, times(1)).rejectValue(eq("login"), eq("incorrect"), anyObject());
		verify(errors, times(1)).rejectValue(anyObject(), anyObject(), anyObject());
	}
	
	
	@Test
	public void emptyFullnameTest() {
		User user = new UserBuilder()
				.setFullname("")
				.setLogin("123")
				.setPassword("12345")
				.build();
		
		validator.validate(user, errors);
		
		verify(errors, times(1)).rejectValue(eq("fullname"), eq("required"), anyObject());
		verify(errors, times(1)).rejectValue(anyObject(), anyObject(), anyObject());
	}
	

	@Test
	public void incorrectFullnameTest() {
		User user = new UserBuilder()
				.setFullname("123")
				.setLogin("12345")
				.setPassword("12345")
				.build();
		
		validator.validate(user, errors);
		
		verify(errors, times(1)).rejectValue(eq("fullname"), eq("incorrect"), anyObject());
		verify(errors, times(1)).rejectValue(anyObject(), anyObject(), anyObject());
	}
	
	
	@Test
	public void emptyPasswordTest() {
		User user = new UserBuilder()
				.setFullname("12345")
				.setLogin("123")
				.setPassword("")
				.build();
		
		validator.validate(user, errors);
		
		verify(errors, times(1)).rejectValue(eq("password"), eq("required"), anyObject());
		verify(errors, times(1)).rejectValue(anyObject(), anyObject(), anyObject());
	}
	
	@Test
	public void incorrectPasswordTest() {
		User user = new UserBuilder()
				.setFullname("123456")
				.setLogin("12345")
				.setPassword("1234")
				.build();
		
		validator.validate(user, errors);
		
		verify(errors, times(1)).rejectValue(eq("password"), eq("incorrect"), anyObject());
		verify(errors, times(1)).rejectValue(anyObject(), anyObject(), anyObject());
	}
}




